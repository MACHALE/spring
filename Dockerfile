FROM maven

RUN java -version

RUN  mvn --version

WORKDIR /usr/src/app
COPY . .
RUN ls
RUN mvn -version
RUN mvn clean install

EXPOSE 8080
ENTRYPOINT ["java","-jar","/root/.m2/repository/com/sample/java/sample/0.0.1-SNAPSHOT/sample-0.0.1-SNAPSHOT.jar"]
